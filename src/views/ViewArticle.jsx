import React,{useState,useEffect} from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import {useParams} from 'react-router'
import { fetchArticleById } from '../services/article_service'
function ViewArticle() {

  const [article, setArticle] = useState()
  const {id} = useParams()

  useEffect(() => {
    if(id){
      fetchArticleById(id).then(article=>{
        setArticle(article)
      })
    }
  }, [])


  return (
    <Container>
        {article ? <>
            <h1 className="my-2">{article.title}</h1>
            <h3>category: {article.category? article.category.name : 'no category'}</h3>
           <Row>
             <Col md={8}>
                  <p>{article.description}</p>
             </Col>
             <Col md={4}>
               <img className="w-100" src={article.image?article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}/>
             </Col>
           </Row>
        </>:
        ''}
    </Container>
  )
}

export default ViewArticle
