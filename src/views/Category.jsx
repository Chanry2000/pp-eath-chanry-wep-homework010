import React, { useEffect, useState } from "react";
import { Container, Form, Table, Button } from "react-bootstrap";
import { useHistory } from "react-router";
import { addCategory, deleteCategoryById, fetchAllCategory, fetchCategoryById, updateCategoryById } from '../services/category_service';
import { useLocation } from "react-router";
import query from 'query-string'

const Category = () => {

    const [category, setCategory] = useState([]);
    const history = useHistory();
    const [name, setName] = useState("");

    const { search } = useLocation()
    console.log("Location:", search);

    let { id } = query.parse(search)
    console.log("id query:", id);


    async function onAddCategory() {

        if (search == "") {
            const category = {
                name,
            };
            const result = await addCategory(category);
        } else {
            const category = {
                name,
            };

            const result = await updateCategoryById(id, category)
        }
        const result = await fetchAllCategory();
        console.log("Category:", result);
        setCategory(result);
        setName("")
        history.push("")
    }

    useEffect(async () => {
        const result = await fetchAllCategory();
        console.log("Category:", result);
        setCategory(result);
        if (search == "") {
            setName("")
        } else {
            const result = await fetchCategoryById(id)
            setName(result.name)
        }
    }, [id]);

    
    async function onDeleteCatById(id) {
        const result = await deleteCategoryById(id);
        const temp = category.filter(item => {
            return item._id != id
        })
        setCategory(temp)
    }
    return (
        <Container>
            <h1 className="my-3">Category</h1>
            <Form>
                <Form.Group controlId="category">
                    <Form.Label>Category Name</Form.Label>
                    <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Category Name" />
                    <br />
                    <Button onClick={onAddCategory} variant="primary">{search ? "Update" : "Add"}</Button>
                    <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
            </Form>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {category.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{item._id.slice(0, 8)}</td>
                                <td>{item.name}</td>
                                <td>
                                    <Button onClick={() => history.push(`/?id=${item._id}`)} variant="warning" size="sm">Edit</Button>{' '}
                                    <Button onClick={() => onDeleteCatById(item._id)} variant="danger" size="sm">Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </Container>
    );
};

export default Category;