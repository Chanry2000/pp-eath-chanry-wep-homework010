import React, { useState, useEffect } from "react";
import {Container,Table,Button,Col,Row,Form,Image,ButtonGroup,} from "react-bootstrap";
import {fetchAllAuthors,putAuthor,postAuthor,deleteAuthorById,uploadImage,} from "../services/author_service";

const defaultImage =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/1200px-Picture_icon_BLACK.svg.png";

 function Author() {
  const [authors, setAuthors] = useState([]);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageUrl, setImageUrl] = useState(defaultImage);
  const [imageFile, setImageFile] = useState(null);
  const [isUpdating, setIsUpdating] = useState(false);
  const [needsReloading, setNeedsReloading] = useState(false);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [providedWrongEmail, setProvidedWrongEmail] = useState(false);



  useEffect(() => {
    const fetch = async () => {
      const results = await fetchAllAuthors();
      setAuthors(results);
    };
    fetch();
    if (needsReloading) setNeedsReloading(false);
  }, [needsReloading]);

  //Validate Email
  const isCorrectEmail = (email) => {
    const emailPattern = /^([a-zA-Z0-9\W]+)@(\w+)(\.([a-z]{2,5})){1,2}$/g;
    return emailPattern.test(email);
  };

  const clearForm = (e) => {
    e.preventDefault();

    setId("");
    setName("");
    setEmail("");
    setImageUrl(defaultImage);
    setImageFile("");
  };


  
  const setEmailWithValidation = (emailInput) => {
    setEmail(emailInput);
    let valid = isCorrectEmail(emailInput);
    setIsValidEmail(valid);
    if (valid) setProvidedWrongEmail(true);

    if (providedWrongEmail) {
      if (!valid) {
        console.log("Please provide a valid email");
      } else {
        setProvidedWrongEmail(false);
        setIsValidEmail(true);
      }
    }
  };

  //Add button
  const onAdd = async (e) => {
    e.preventDefault();

    if (isCorrectEmail(email)) {
      const tempAuthor = {
        name,
        email,
      };

      if (imageFile) {
        let url = await uploadImage(imageFile);
        tempAuthor.image = url;
        console.log(imageFile);
      }

      if (isUpdating) {
        putAuthor(id, tempAuthor).then((message) => alert(message));
        setIsUpdating(false);
        setNeedsReloading(true);
      } else {
        postAuthor(tempAuthor).then((message) => alert(message));
        setNeedsReloading(true);
      }

      clearForm(e);
      setIsUpdating(false);
    } else {
      setProvidedWrongEmail(true);
    }
  };

    //Edit button
  const onEdit = async (e, id, name, email, imgUrl) => {
    e.preventDefault();
    if (isUpdating) {
      clearForm(e);
      setIsUpdating(false);
    } else {
      setNeedsReloading(true);
      setIsUpdating(true);
      setId(id);
      setName(name);
      setEmail(email);
      setImageUrl(imgUrl);
    }
  };

  //Delete button
  const onDelete = async (e, id) => {
    e.preventDefault();
    setNeedsReloading(true);
    deleteAuthorById(id).then((message) => alert(message));
    const temp = authors.filter((author) => {
      return author._id !== id;
    });
    setAuthors(temp);
  };


 


  return (
    <div>
      <Container className="my-2">
        <Col>
          <Row className={"mb-4"}>
            <Col xs={8}>
              <Form style={{ width: "auto" }}>
                <div><h2>Author</h2></div><br/>
                <Form.Group controlId='formBasicName'>
                  <Form.Label>Author Name</Form.Label>
                  <Form.Control type='text' placeholder='Author Name'  onChange={(e) => setName(e.target.value)} value={name} />
                </Form.Group>
                <Form.Group controlId='formBasicEmail' className={"mt-3"}>
                  <Form.Label>Email</Form.Label>
                  <Form.Control  type='email'  placeholder='Email'  onChange={(e) => setEmailWithValidation(e.target.value)}  value={email}  />
                </Form.Group>
                <Form.Text
                  style={{ color: "red", display: "block" }}
                  hidden={isValidEmail || !providedWrongEmail}>
                  Please provide a valid email
                </Form.Text>
               ]
                <ButtonGroup className={"my-3"} aria-label='Basic example'>
                  <Button variant='primary' type='submit' onClick={(e) => onAdd(e)}> {isUpdating ? "Save" : "Add"} </Button>
                  <Button hidden={!isUpdating} variant='secondary' type='submit' onClick={(e) => { clearForm(e); setIsUpdating(false); }}>Cancel</Button>
                </ButtonGroup>
                ]
              </Form>
            </Col>

            <Col xs={4} className={"mt-4"}>
              <Image src={imageUrl} className={"w-100"} />
              <Form>
                <Form.File  id='img'  className={"py-2"}  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageUrl(url);
                  }}
                />
              </Form>
            </Col>
          </Row>

          <Table bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {authors.map((author, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>     
                  <td>{author.name}</td>
                  <td>{author.email}</td>
                  <td>
                    <Image src={author.image} className={"w-25"} />
                  </td>
                  <tr>
                  <td>
                    <Button  size="sm"   variant='warning'  onClick={(e) =>  onEdit(e,  author._id,  author.name,  author.email,  author.image  )  }>
                      {isUpdating && id === author._id ? "Cancel" : "Edit"}
                    </Button>{' '}
                    <Button  size="sm"   disabled={isUpdating}  variant='danger'  onClick={(e) => onDelete(e, author._id)}>Delete</Button>
                  </td>
                  </tr>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Container>
    </div>
  );
}
export default Author;