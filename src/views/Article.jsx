import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Card } from "react-bootstrap";
import { useHistory } from "react-router";
import { fetchArticle,deleteArticle } from "../services/article_service";

function Article() {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      let articles = await fetchArticle();
      setArticles(articles);
    };
    fetch();
  }, []);

  const history = useHistory()


  const onDelete = (id)=>{
    deleteArticle(id).then((message)=>{

      let newArticles = articles.filter((article)=>article._id !== id)
      setArticles(newArticles)

      alert(message)
    }).catch(e=>console.log(e))
  }


  let articleCard = articles.map((article) => (
    <Col md={3} key={article._id}>
      <Card className="my-2">
        <Card.Img
          variant="top"
          style={{ objectFit: "cover", height: "150px" }}
          src={article.image ? article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
        />
        <Card.Body>
          <Card.Title>{article.title}</Card.Title>
          <Card.Text className="text-line-3">
           {article.description}
          </Card.Text>
          <Button   size="sm"   variant="primary"  onClick={()=>  history.push('/article/'+article._id)  }>View</Button>{" "}
          <Button   size="sm"   variant="warning"  onClick={()=>{  history.push('/update/article/'+article._id)  }}  >Edit</Button>{" "}
          <Button size="sm" variant="danger" onClick={()=>onDelete(article._id)}>Delete</Button>
        </Card.Body>
      </Card>
    </Col>
  ));
  return (
    <Container>
      <h1 className="text-center my-4">AMS Management</h1>
      <Row>{articleCard}</Row>
    </Container>
  );
}

export default Article;
