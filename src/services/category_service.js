import api from "../api/api"

export const fetchCategory = async () => {
    let response = await api.get('category')
    return response.data.data

}

//Fetch all category
export const fetchAllCategory = async () => {
  try {
    const result = await api.get("/category")
    console.log("result Category", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("error Category", error);
  }
}

//Fetch category by id
export const fetchCategoryById = async (id) => {
  try {
    const result = await api.get(`/category/${id}`)
    console.log("fetchCategoryById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("category error:", error);
  }
}

//Add category to api
export const addCategory = async (category) => {
  try {
    const result = await api.post("/category", category);
    console.log("addCategory:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("addArticle Error:", error);
  }
}

//Update category by ID
export const updateCategoryById = async (id, category) => {
  try {
    const result = await api.put(`/category/${id}`, category)
    console.log("updateCategoryById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("updateCategoryById Error:", error);
  }
}

//Delete category by ID
export const deleteCategoryById = async (id) => {
  try {
    const result = await api.delete(`/category/${id}`);
    console.log("deleteCatById:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("deleteCatById Error:", error);
  }
}